
# Logic RPN

A programming language written in reverse polish notation that operates a stack machine.
It uses only letters and numbers and cannot syntax fail.


## Implementation Status

Everything implemented except numeric operators.


## General Procedure

Several special characters are functions which can take an arbitrary number of args, including 0.
Functions consume arguments if they can, up to a function specific default limit.
Numbers after functions alter them via their duplication mode, either altering the number of arguments or the number of outputs, sometimes both.
Functions return their arguments back to the stack after they return their result, unless they are in capitalised form in which case they leave them consumed.
Characters which are not in the function table reference register state.
Variables left on stack at end of program are treated as outputs.


## Primitive Functions / Stack Manipulation

| Symbol | Default n-in | Default n-out | Operation          | Duplication mode | Default Dupe |
|:------:|:------------:|:-------------:|:-------------------|:-----------------|:------------:|
| `c`    | 1            | 0             | Null               | Naive            | 1            |
| `i`    | 0            | 1             | Std In  (Base64)   | Naive            | 1            |
| `w`    | 1            | 0             | Std Out (Base64)   | Naive            | 1            |
| `t`    | 0            | 1             | Const true         | Naive            | 1            |
| `f`    | 0            | 1             | Const false        | Naive            | 1            |
| `r`    | 0            | 0             | Rotate stack right | Naive            | 1            |
| `l`    | 0            | 0             | Rotate stack left  | Naive            | 1            |
| `z`    | 0            | 0             | Flip stack         | Across Input     | 1            |
| `k`    | 0            | 0             | Replicate stack    | Across Input     | 1            |
| `d`    | 0            | 0             | Duplicate stack    | Effect Scaling   | 1            |
| `g`    | 1            | 0             | Cond go forward    | Effect Scaling   | 2            |
| `b`    | 1            | 0             | Cond go back       | Effect Scaling   | 2            |
| `j`    | 1            | 0             | Set reg bank       | Across Input     | 1            |
| `n`    | 1            | 1             | Not                | Parallel N-to-N  | 1            |
| `u`    | 2            | 1             | And (All)          | Across Input     | 1            |
| `e`    | 2            | 1             | Or (Exists)        | Across Input     | 1            |
| `v`    | 2            | 1             | Exactly 1          | Across Input     | 1            |
| `p`    | 2            | 1             | Parity             | Across Input     | 1            |
| `a`    | 2            | 1             | Parallel 2-and     | Across Input     | 1            |
| `o`    | 2            | 1             | Parallel 2-or      | Across Input     | 1            |
| `x`    | 2            | 1             | Parallel 2-xor     | Across Input     | 1            |


### Notes

`C` is the conusming null op, so can be used to delete things from the stack.
Naive duplication means equivalent to copying the symbol that many times.
Rotate right will send top element of stack to bottom, rotate left bottom element to top.
This is because the stack is represented as a list, where the head element is leftmost.
Flip completely inverts the stack.
User input is cached and consumed when needed, more is asked for when cache has run out.
This allows user to input lots of data even if only a little bit is required.
Go commands jump their duplication if their arg is 1.
This is instead of usual PC increase, so single jump is a nop.
For `j` usage see register state.
`k` adds one copy of duplication-many elements of the stack to itself.
`d` adds duplication-many copies of the entire stack to itself.
The characters `hmqsy` are left for register state (see below).


## Numbers and Numeric Operators

| Symbol | Value           |
|:------:|:----------------|
| `0-9`  | Literal         |
| `+`    | Stack Size      |
| `/`    | Program Counter |

Numbers at the start of the program are used to initialise the stack via their conversion from base10 to base2.
After this, they will always follow an instruction and are thus used to "duplicate" this instruction, though the exact semantics of this varies depending on the instruction.
Numeric operators allow various values to be used for duplication.
They add any number to their left and subtract any to the right.
They are left associative.


## Register State

State registers are accessed for read/write by typing lowercase/capital letters that are not in the function table.
Each register is itself a stack of values, and the duplication on the read/write determines how many bits to read/write, counting from the top down of both stacks.
Both read/write operations copy by value.
Writing always consumes input without replacement, since a subsequent read can put the values back on the working stack.
Registers are organised in banks.
Each leftover letter represents a register in each bank.
The bank of registers that are used for each read/write is determined by the value of the reg offset, this can be written from the stack via the `j` instruction, which copies a number of bits equal to its duplication.


## Todo

### Spec

* Flush output buffer cmd?
* More crazy / wacky things like random inputs? Meta-programming / syntactic sugar?
* Detection of non-halting in simple cases? Watch stack and if it's been the same as previous some number of times then check if instruction pos matches? If not reset count and multiply threshold? Watch program counter first to avoid storing lots of stack images!

### Implementation

* More cmd args
    * Log printing flags
    * Encoding (specified fixed bit numeric vs base64)
    * Decoding stack for final output
* Numeric operations

### Misc

* Examples


## Potential Ideas

### User Defined Functions / More Heap Memory

The user can define functions to be later re-used.
This can also function as heap memory storage in the case of constant functions.
When a `:` is found, all following letters/numbers until the next `:` are used to name our heap entry.
Then the following letters/numbers until a `.` define the code of our function.
Function definitions can be nested in the code of other functions.
Functions can be accessed via `!name.`, which causes that reference to be simply replaced by the stored code.
Functions can be called inside the names of other functions (hehe).
Calling a function that does not exist is ignored (i.e. functions default to no-op's).
Functions can be overwritten.

