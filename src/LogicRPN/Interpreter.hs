{-# LANGUAGE FlexibleContexts #-}

module LogicRPN.Interpreter
  ( InterpreterM
  , runInterpreter
  , interpreterMain
  , printLogs
  ) where


import LogicRPN.Core
  ( ProgramState (..), Operation (..), Primitive (..), RegMode (..)
  , execQueue, execPrev, programStack, currentRegBank, registers, inputBuffer, outputBuffer
  , boolToChar, boolToInt, bitsToInt, xor, b64Alphabet, b64decoder )

import qualified Data.Map as M

import Data.List.Extra (splitAtEnd)
import Data.Tuple (swap)
import Data.Tuple.Extra (first)
import Data.Maybe (fromMaybe)

import Control.Lens ((.=), (%=), use)
import Control.Monad (when)
import Control.Monad.Extra (whenM)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.State.Lazy (MonadState, StateT, execStateT)
import Control.Monad.Writer (MonadWriter, WriterT, runWriterT, tell)


type InterpreterM a = (StateT ProgramState) (WriterT StateLog IO) a

-- StateLog: stack input output
data StateLog = StateLog [[Bool]] [[Bool]] [[Bool]]

instance Semigroup StateLog where
  (StateLog sL1 iL1 oL1) <> (StateLog sL2 iL2 oL2) = StateLog (sL1<>sL2) (iL1<>iL2) (oL1<>oL2)

instance Monoid StateLog where
  mempty = StateLog [] [] []


runInterpreter :: InterpreterM () -> ProgramState -> IO ([Bool], [Bool], StateLog)
runInterpreter interpM iniState = f <$> (runWriterT . execStateT interpM) iniState
  where f (x, y) = (_programStack x, _outputBuffer x, y)


interpreterMain :: (MonadState ProgramState m, MonadWriter StateLog m, MonadIO m) => m ()
interpreterMain = do
  logState
  nextOp <- getNextOp

  case nextOp of
    Nothing -> return ()
    Just op -> execOp op >> interpreterMain


logState :: (MonadState ProgramState m, MonadWriter StateLog m) => m ()
logState = do
  stack <- use programStack
  inBuff <- use inputBuffer
  outBuff <- use outputBuffer
  tell (StateLog [stack] [inBuff] [outBuff])


getNextOp :: (MonadState ProgramState m) => m (Maybe Operation)
getNextOp = do
  ops <- use execQueue
  case ops of
    []                 -> return Nothing
    (op:remaining_ops) -> do
      execQueue .= remaining_ops
      execPrev %= (:) op
      return (Just op)


execOp :: (MonadState ProgramState m, MonadIO m) => Operation -> m ()

execOp (PrimOp StdIn       _   n) = do
  checkFillInputBuffer n
  bits <- take n <$> use inputBuffer
  inputBuffer %= drop n
  programStack %= (++) (reverse bits)

execOp (PrimOp StdOut      csm n) = do
  bits <- stackCsmTake csm n
  outputBuffer %= (++ bits)
  checkEmptyOutputBuffer

execOp (PrimOp Nop         csm n) = programStack %= if csm then drop n else id
execOp (PrimOp ConstTrue   _   n) = programStack %= (++) (replicate n True)
execOp (PrimOp ConstFalse  _   n) = programStack %= (++) (replicate n False)
execOp (PrimOp RotateRight _   n) = programStack %= uncurry (++) . swap . splitAt n
execOp (PrimOp RotateLeft  _   n) = programStack %= uncurry (++) . swap . splitAtEnd n

execOp (PrimOp Flip        csm n) = do
  bits <- stackCsmTake csm n
  programStack %= (++) (reverse bits)

execOp (PrimOp Replicate   _   n) = programStack %= concat . replicate n
execOp (PrimOp Duplicate   _   n) = programStack %= (\bits -> take n bits ++ bits)
execOp (PrimOp Not         csm n) = programStack %= parallelUnaryOp not n csm
execOp (PrimOp All         csm n) = programStack %= nAryOp and n csm
execOp (PrimOp Exists      csm n) = programStack %= nAryOp or n csm
execOp (PrimOp ExactlyOne  csm n) = programStack %= nAryOp ((==) 1 . sum . fmap boolToInt) n csm
execOp (PrimOp Parity      csm n) = programStack %= nAryOp (odd . sum . fmap boolToInt) n csm
execOp (PrimOp And         csm n) = programStack %= parallelBinaryOp (&&) n csm
execOp (PrimOp Or          csm n) = programStack %= parallelBinaryOp (||) n csm
execOp (PrimOp Xor         csm n) = programStack %= parallelBinaryOp xor n csm

execOp (PrimOp CondForward csm n) = do
  flag <- stackCsmTake csm 1
  let k = n - 1  -- As we've alread jumped forward one in getNextOp
  when (or flag) (do  -- flag is either [] or [aBool], using `or` defaults to false if []
    ops <- take k <$> use execQueue
    execQueue %= drop k
    execPrev %= (++) (reverse ops) )

execOp (PrimOp CondBack csm n) = do
  flag <- stackCsmTake csm 1
  let k = n + 1
  when (or flag) (do
    ops <- take k <$> use execPrev
    execPrev %= drop k
    execQueue %= (++) (reverse ops) )

execOp (PrimOp SetRegBank csm n) = do
  bits <- stackCsmTake csm n
  currentRegBank .= bitsToInt bits

execOp (RegOp reg Read n) = do
  i <- use currentRegBank
  regs <- use registers
  let reg_contents = fromMaybe [] (M.lookup (i, reg) regs)
      read_bits = take n $ reg_contents ++ repeat False
  programStack %= (++) read_bits

execOp (RegOp reg Write n) = do
  i <- use currentRegBank
  bits <- stackCsmTake True n
  registers %= M.insert (i, reg) bits


stackCsmTake :: (MonadState ProgramState m) => Bool -> Int -> m [Bool]
stackCsmTake csm n = do
  bits <- take n <$> use programStack
  when csm (programStack %= drop n)
  return bits

checkFillInputBuffer :: (MonadState ProgramState m, MonadIO m) => Int -> m ()
checkFillInputBuffer n = do
  whenM ((< n) . length <$> use inputBuffer) (do
    user_inp <- liftIO getChar
    let bits = b64decoder [user_inp]
    inputBuffer %= (++ bits)
    checkFillInputBuffer n )

checkEmptyOutputBuffer :: (MonadState ProgramState m, MonadIO m) => m ()
checkEmptyOutputBuffer = do
  out_bits <- take 6 <$> use outputBuffer
  when (length out_bits == 6) (do
    outputBuffer %= drop 6
    liftIO $ putChar (b64Alphabet !! bitsToInt out_bits)
    checkEmptyOutputBuffer )


parallelUnaryOp :: (Bool -> Bool) -> Int -> Bool -> [Bool] -> [Bool]
parallelUnaryOp op n csm bits = if csm then new ++ remaining else args ++ new ++ remaining
  where new = op <$> args
        (args, remaining) = splitAt n bits

parallelBinaryOp :: (Bool -> Bool -> Bool) -> Int -> Bool -> [Bool] -> [Bool]
parallelBinaryOp op n csm bits = if csm then new ++ remaining else args ++ new ++ remaining
  where new = uncurry op <$> zip word1 (word2 ++ repeat False)  -- Need to pad second word
        (args, remaining) = splitAt (2*n) bits
        (word1, word2) = splitAt n args

nAryOp :: ([Bool] -> Bool) -> Int -> Bool -> [Bool] -> [Bool]
nAryOp op n csm bits = if csm then new : remaining else args ++ [new] ++ remaining
  where new = op args
        (args, remaining) = splitAt n bits


printLogs :: StateLog -> IO ()
printLogs (StateLog sL _ _) = do
  putStrLn "Stack Log:"
  mapM_ (putStrLn . formatBits) sL
  -- TODO: Make printing of each log its own separate cmd line arg
--  putStrLn "Input Buffer Log:"
--  mapM_ (putStrLn . formatBits) iL
--  putStrLn "Output Buffer Log:"
--  mapM_ (putStrLn . formatBits) oL


formatBits :: [Bool] -> String
formatBits bits = "  " ++ if null bits then "Empty" else boolToChar <$> bits

