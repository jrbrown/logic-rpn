
module LogicRPN.ArgParse
  ( Config (..)
  , ProgramSource (..)
  , parseArgs
  ) where

import Options.Applicative


data Config = Config
  { _programSource :: ProgramSource
  , _onlyPrintOutput :: Bool
  , _flushStackToOutputAtEnd :: Bool
  , _ignoreStdCmds :: Bool
  , _parsedProgramPrintFlag :: Bool
  , _logPrintFlag :: Bool }

data ProgramSource = File String | StdIn


parseArgs :: IO Config
parseArgs = execParser argParser


argParser :: ParserInfo Config
argParser = info (configP <**> helper)
            (  fullDesc
            <> progDesc "Interpreter for the Logic RPN language (name WIP)."
            <> header "Logic RPN Interpreter" )


configP :: Parser Config
configP = Config
      <$> programSourceP
      <*> switch
          (  long "output_only"
          <> short 'o'
          <> help "Only print outputted data, overrides other printing flags" )
      <*> switch
          (  long "flush_stack"
          <> short 'f'
          <> help "Flush stack to output at end of program" )
      <*> switch
          (  long "disable_io"
          <> short 'd'
          <> help "Disable StdIn and StdOut instructions" )
      <*> switch
          (  long "print_parse"
          <> short 'p'
          <> help "Print the parsed program" )
      <*> switch
          (  long "print_logs"
          <> short 'l'
          <> help "Print the program logs" )


programSourceP :: Parser ProgramSource
programSourceP = ( File
                 <$> argument str
                     (  metavar "SOURCE"
                     <> help "File to load program data from"))
                 <|> flag' StdIn
                     (  long "stdin"
                     <> short 'i'
                     <> help "Load program from StdIN" )

