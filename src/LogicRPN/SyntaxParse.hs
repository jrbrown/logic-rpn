
module LogicRPN.SyntaxParse
  ( parseSyntax
  ) where


import LogicRPN.Core
  ( Operation (..), Primitive (..), Register (..), RegMode (..), Duplication)

import Data.Char (toLower, toUpper)
import Data.Maybe (catMaybes)

import Text.Parsec (parse, many, many1, choice, char, option, digit, anyToken)
import Text.Parsec.String (Parser)

import Control.Applicative ((<|>))


parseSyntax :: String -> Bool -> [Operation]
parseSyntax syntax ignoreIOCmds =
  case parse syntaxP "Program Source" syntax of
    Left e  -> error $ show e
    Right x -> if ignoreIOCmds then filter f x else x
      where f (PrimOp StdIn  _ _) = False
            f (PrimOp StdOut _ _) = False
            f _                   = True


syntaxP :: Parser [Operation]
syntaxP = catMaybes <$> many
                        (   Just <$> primitiveOpP
                        <|> Just <$> regOpP
                        <|> (anyToken >> return Nothing) )

primitiveOpP :: Parser Operation
primitiveOpP = choice [ genericPrimOpP 'c' Nop
                      , genericPrimOpP 'i' StdIn
                      , genericPrimOpP 'w' StdOut
                      , genericPrimOpP 't' ConstTrue
                      , genericPrimOpP 'f' ConstFalse
                      , genericPrimOpP 'r' RotateRight
                      , genericPrimOpP 'l' RotateLeft
                      , genericPrimOpP 'z' Flip
                      , genericPrimOpP 'k' Replicate
                      , genericPrimOpP 'd' Duplicate
                      , genericPrimOpP 'g' CondForward
                      , genericPrimOpP 'b' CondBack
                      , genericPrimOpP 'j' SetRegBank
                      , genericPrimOpP 'n' Not
                      , genericPrimOpP 'u' All
                      , genericPrimOpP 'e' Exists
                      , genericPrimOpP 'v' ExactlyOne
                      , genericPrimOpP 'p' Parity
                      , genericPrimOpP 'a' And
                      , genericPrimOpP 'o' Or
                      , genericPrimOpP 'x' Xor ]

genericPrimOpP :: Char -> Primitive -> Parser Operation
genericPrimOpP c prim = PrimOp prim
                    <$> ((char (toLower c) >> return False) <|> (char (toUpper c) >> return True))
                    <*> duplicationP (primDupeDefault prim)

primDupeDefault :: Primitive -> Int
primDupeDefault op = if op `elem` [CondForward, CondBack] then 2 else 1

regOpP :: Parser Operation
regOpP = choice [ genericRegOpP 'h' Reg1
                , genericRegOpP 'm' Reg2
                , genericRegOpP 'q' Reg3
                , genericRegOpP 's' Reg4
                , genericRegOpP 'y' Reg5 ]

genericRegOpP :: Char -> Register -> Parser Operation
genericRegOpP c reg = RegOp reg
                  <$> ((char (toLower c) >> return Read) <|> (char (toUpper c) >> return Write))
                  <*> duplicationP 1

duplicationP :: Duplication -> Parser Duplication
duplicationP stnd = option stnd (read <$> many1 digit)

