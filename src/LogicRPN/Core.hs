{-# LANGUAGE TemplateHaskell #-}

module LogicRPN.Core
  ( ProgramState (..)
  , Operation (..)
  , Primitive (..)
  , EatArgsFlag, Duplication
  , RegMode (..)
  , Register (..)
  , execQueue, execPrev, programStack, currentRegBank, registers, inputBuffer, outputBuffer
  , loadProgram
  , boolToChar, charToBool, boolToInt, intToBool, bitsToInt, intToBits
  , takePadEnd, takePadStart
  , xor
  , b64Alphabet, b64encoder, b64decoder
  ) where


import Data.Map (Map, empty)
import Data.List (elemIndex)

import Control.Lens (makeLenses)


data ProgramState = ProgramState
  { _execQueue :: [Operation]
  , _execPrev :: [Operation]
  , _programStack :: [Bool]
  , _currentRegBank :: Int
  , _registers :: Map (Int, Register) [Bool]
  , _inputBuffer :: [Bool]
  , _outputBuffer :: [Bool] }

data Operation = PrimOp Primitive EatArgsFlag Duplication
               | RegOp Register RegMode Duplication
               deriving (Eq, Ord, Show)

data Primitive = StdIn
               | StdOut
               | Nop
               | ConstTrue
               | ConstFalse
               | RotateRight
               | RotateLeft
               | Flip
               | Replicate
               | Duplicate
               | CondForward
               | CondBack
               | SetRegBank
               | Not
               | All
               | Exists
               | ExactlyOne
               | Parity
               | And
               | Or
               | Xor
               deriving (Eq, Ord, Show)

type EatArgsFlag = Bool
type Duplication = Int

data RegMode = Read | Write deriving (Eq, Ord, Show)
data Register = Reg1 | Reg2 | Reg3 | Reg4 | Reg5 deriving (Eq, Ord, Show)

makeLenses ''ProgramState


loadProgram :: [Operation] -> ProgramState
loadProgram ops = ProgramState ops [] [] 0 empty [] []

boolToChar :: Bool -> Char
boolToChar True  = '1'
boolToChar False = '0'

charToBool :: Char -> Maybe Bool
charToBool '1' = Just True
charToBool '0' = Just False
charToBool _   = Nothing

boolToInt :: Bool -> Int
boolToInt True  = 1
boolToInt False = 0

intToBool :: Int -> Maybe Bool
intToBool 1 = Just True
intToBool 0 = Just False
intToBool _ = Nothing

bitsToInt :: [Bool] -> Int
bitsToInt bits = sum $ zipWith f (boolToInt <$> reverse bits) [0..]
  where f :: Int -> Int -> Int
        f x y = x * (2^y)

intToBits :: Int -> [Bool]
intToBits n = reverse $ intToBits' n
  where intToBits' 0 = []
        intToBits' k = odd k : intToBits' (k `div` 2)

takePadEnd :: Int -> a -> [a] -> [a]
takePadEnd n p xs = take n (xs ++ repeat p)

takePadStart :: Int -> a -> [a] -> [a]
takePadStart n p xs = reverse (takePadEnd n p (reverse xs))

xor :: Bool -> Bool -> Bool
xor False False = False
xor False True  = True
xor True  False = True
xor True  True  = False

b64Alphabet :: [Char]
b64Alphabet = ['A'..'Z'] ++ ['a'..'z'] ++ ['0'..'9'] ++ "+/"

b64encoder :: [Bool] -> [Char]
b64encoder []   = []
b64encoder bits = (b64Alphabet !! bitsToInt (takePadEnd 6 False bits)) : b64encoder (drop 6 bits)

b64decoder :: [Char] -> [Bool]
b64decoder = concatMap (maybe [] (takePadStart 6 False . intToBits) . flip elemIndex b64Alphabet)

