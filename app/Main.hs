
module Main
  ( main
  ) where


import LogicRPN.Core (loadProgram, b64encoder, boolToChar)
import LogicRPN.ArgParse (Config (..), ProgramSource (..), parseArgs)
import LogicRPN.Interpreter (runInterpreter, interpreterMain, printLogs)
import LogicRPN.SyntaxParse (parseSyntax)

import Control.Monad (when, unless)


main :: IO ()
main = do
  Config programSource onlyPrintOutput flushStackAtEnd ignoreIOCmds printOps printLogsFlag <- parseArgs

  programSyntax <- case programSource of
                     File srcPath -> readFile srcPath
                     StdIn        -> getLine

  let programOps = parseSyntax programSyntax ignoreIOCmds

  when (printOps && not onlyPrintOutput) $ do
    putStrLn "Parsed Program:"
    mapM_ (\x -> putStrLn ("  " ++ show x)) programOps

  unless onlyPrintOutput (putStrLn "Program-User Interaction:")
  (finalStack, finalOutputBuffer, logs) <- runInterpreter interpreterMain (loadProgram programOps)
  unless onlyPrintOutput (putStrLn "")

  when (printLogsFlag && not onlyPrintOutput) (printLogs logs)

  when flushStackAtEnd (putStrLn (b64encoder (finalOutputBuffer ++ finalStack)))

  unless (onlyPrintOutput || flushStackAtEnd) (do
    putStrLn "Final Stack State:"
    putStrLn (boolToChar <$> finalStack) )

